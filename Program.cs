/*Cameron Deao
 * CST-227
 * James Shinevar
 * 7/7/2019
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-5/src/master/ */

using Cameron_Deao_Milestone_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Cameron_Deao_Milestone_2
{
    class Program
    {
        //Updated the main method to display the proper windows in the proper
        //order of appearance.
        static void Main()
        {
            ChoosingDifficulty choosingDifficulty = new ChoosingDifficulty();
            Application.Run(choosingDifficulty);
            int size = choosingDifficulty.GridSize();
            Grid newGrid = new Grid();
            newGrid.SetupButtons(size);
            Application.Run(newGrid);
        }
    }
}